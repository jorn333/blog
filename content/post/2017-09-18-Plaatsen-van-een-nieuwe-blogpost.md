---
title: Mijn Blog
subtitle: Blog kwartaal 1
date: 2017-09-18
---

Logboek

18 sep 2017

Vandaag was mijn eerste dag op de nieuwe opleiding ik heb Lorenzo ontmoet hij zit in mijn groepje. Hij heeft het eerste concept aan mij gepresenteerd en een aantal schetsen. Zagen er goed uit. Vervolgens heb ik actief meegedacht over eventuele verbeteringen.

Zo heb ik bedacht het spel in ‘districten te verdelen. Zo wordt het spel overzichtelijk en krijgt de speler van onze game snelle succeservaringen doordat hij/zij die districten kan veroveren. Dit doen zij door een aantal opdrachten te voltooien per district. 

Als deze opdrachten afgerond zijn heeft de speler een district veroverd. Op deze manier verover je heel Rotterdam maar leert de speler het gebied ook kennen.


S middags hebben we de opdracht gekregen om de ‘kill your darling’ techniek toe te passen. We zijn gaan brainstormen.  Hebben hier het volgende uitgehaald:

20 sep 2017

Vandaag waren onze andere teamgenoten er weer bij Petronette en Chrissleen. Met hun hebben we opnieuw een brainstormsessie gehouden voor het spel. We hebben dit gedaan aan de hand van de HKJ methode. 


HKJ methode is een methode waarbij je doormiddel van jezelf verschillende vragen te stellen antwoorden formuleert op die vragen. Wij hebben dit gedaan met behulp van post it papiertjes. Bij ons zag dit er zo uit. 


25 sep 2017

Vandaag zijn we weer door gaan werken met ons idee er moest voor ons nog een teamnaam komen die heb ik bedacht. Purple Dice Studio’s is het geworden. De eerste spelletjes werden gespeeld met een dobbelsteen vandaar de naam achter de kleur zit geen andere betekenis dan dat ik dat een mooie kleur vind.




27 sep 2017

Vandaag zijn we gezamenlijk aan de gang gegaan met het verder uitdenken van de gamen. Tevens hebben we verder gewerkt aan de deliverables die af moesten komen.


2 okt 2017

We hebben voor het uitdenken van onze nieuwe game interviewvragen bedacht zodat we ons nog beter kunnen verdiepen in de doelgroep. Daarnaast hebben we die interview vragen op papier gezet en aan verschillende mensen hun mening gevraagd. 


4 okt 2017

Vandaag moesten wij ons idee presenteren aan een aantal leerlingen en leraren. Hiervoor moesten we een presentatie maken. Deze taak heb ik op me genomen. In deze korte presentatie heb ik inzichtelijk proberen te maken wat ons idee was en hoe we hier op gekomen zijn. Dit was een hele leerzame ervaring want we hebben veel feedback gekregen.



9 okt 2017

Vandaag zijn we tot de conclusie gekomen dat aan de hand van de feedback die we hebben gehad op de presentatie. Ons idee gewoon niet gaat werken. Het idee had teveel losse eindjes en ook voldeed niet alles aan de eisen van de opdrachtgever we hebben afgesproken dat wij allemaal een nieuw idee zouden inbrengen nog voor de lunch. Uiteindelijk hebben we allemaal onze ideeën gepresenteerd en hebben we mijn idee uitgevoerd. Ik heb de spelregels geschreven.


11 okt 2017

Ik ben begonnen met het ontwerpen van een nieuwe spelnaam en logo. ‘Team Up’ is het geworden. Ook heb ik de handleiding weer verder uitgewerkt. En netjes op papier gezet we hebben de laatste afspraken gemaakt over hoe we alles vorm wilden gaan geven.






16 okt 2017

vakantie

18 okt 2017

vakantie




23 okt 2017

we hebben de laatste hand gelegd aan ons spel. Lorenzo heeft alle ontwerpen gemaakt en toen hebben we met zn. allen alles geprint en mooi gemaakt. Op woensdag 25 oktober vindt namelijk de expo plaats en moeten we alles af hebben.


25 okt 2017

vandaag was de Expo ons groepje was op tijd op school om alles neer te zetten we moesten nog aardig wat uitprinten dus dat hebben we gedaan. Toen kon het feest beginnen. Al met al waren we tevreden we hadden een heel clean simplistisch maar enorm leuk spel gemaakt. Toch was het grote publiek meer in voor dingen met meer toeters en bellen. We hebben uiteindelijk 5 stickers gekregen.
